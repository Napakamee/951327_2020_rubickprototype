﻿using UnityEngine;

public class PunSkinSetting
{
    public const string PLAYER_HAIR = "PlayerHair";
    public const string PLAYER_BANG = "PlayerBANG";
    public const string PLAYER_AHOGE = "PlayerAhoge";
    public const string PLAYER_RIGHTHAIR = "PlayerRightHair";
    public const string PLAYER_LEFTHAIR = "PlayerLeftHair";
    public const string PLAYER_BACKHAIR = "PlayerBackHair";
    public const string PLAYER_EYES = "PlayerEyes";
    public const string PLAYER_LEFT_EYE = "PlayerLeftEye";
    public const string PLAYER_RIGHT_EYE = "PlayerRightEye";
    public const string PLAYER_EYESBLOW = "PlayerEyesBlow";
    public const string PLAYER_FACE = "PlayerFace";
    public const string PLAYER_EARS = "PlayerEars";
    public const string PLAYER_UNIFORM = "PlayerUniform";
    public const string PLAYER_BODY = "PlayerBody";
    public const string PLAYER_RIGHT_UPPERARM = "PlayerRightUpperArm";
    public const string PLAYER_RIGHT_FOREARM = "PlayerRightForeArm";
    public const string PLAYER_RIGHTHAND = "PlayerRightHand";
    public const string PLAYER_LAFT_UPPERARM = "PlayerLeftUpperArm";
    public const string PLAYER_Weapon = "PlayerWeapon";
}
