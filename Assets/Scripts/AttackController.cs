﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Serialization;

public class AttackController : MonoBehaviourPun, IPunObservable
{
    //[SerializeField] private Weapons weapon;
    [Header("Objects")] [Tooltip("Use in FollowCursor.")]
    public GameObject weaponHolder;

    public Camera playerCam;
    [Tooltip("Attack Mesh/Sprite.")] 
    public MeshRenderer attackBox;
    [Tooltip("Attack Collider.")] 
    public BoxCollider attackCollider;

    [Space] 
    [SerializeField] private float waitTime;
    [SerializeField] private bool isAttack;
    public WeaponStat weaponStat;

    private float _hideAttackTime = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        playerCam = this.GetComponentInChildren<Camera>();
        attackCollider.enabled = false;
        attackBox.enabled = false;
        isAttack = false;
        waitTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!photonView.IsMine)
            return;

        TimeUpdate();
        //Attack
        if (Input.GetMouseButtonDown(0))
        {
            Attack();
        }
    }

    //Collect Item
    private void OnTriggerStay(Collider other)
    {
        if (!photonView.IsMine)
            return;
        
        if (other.gameObject.CompareTag("Item") && Input.GetKeyDown(KeyCode.F))
        {
            Item item = other.GetComponent<Item>();
            Weapon newWeapon = item.weapon;
            weaponStat.weapon = newWeapon;
            Debug.Log("Collected " + newWeapon.weaponName);
            weaponStat.ChangeWeapon();
            Destroy(other.gameObject);
        }
    }

    private void TimeUpdate()
    {
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            //isAttack = false; //bool for animation
            waitTime = 0;
        }

        //Hide Attack
        if (_hideAttackTime >= 0 && isAttack)
        {
            _hideAttackTime -= Time.deltaTime;
        }

        if (_hideAttackTime <= 0)
        {
            _hideAttackTime = 0.2f;
            attackCollider.enabled = false;
            attackBox.enabled = false;
            isAttack = false;
        }
    }

    private void Attack()
    {
        if (waitTime <= 0)
        {
            //isAttack = true;  //bool for animation
            WeaponFire();
            waitTime = weaponStat.atkSpd;
        }
    }

    private void WeaponFire()
    {
        //object[] data = { photonView.ViewID };
        //attackBox.SetActive(true);
        if (!weaponStat.isRange)//Melee attack
        {
            attackCollider.enabled = true;
            attackBox.enabled = true;
        }
        else //Range attack
        {
            object[] data = {photonView.ViewID};
            GameObject bullet = PhotonNetwork.Instantiate(this.weaponStat.bullet.name,
                weaponHolder.transform.position + (weaponHolder.transform.forward * 1.5f),
                weaponHolder.transform.rotation,
                0,
                data);
            
        }
        
        isAttack = true;
        //Debug.Log("Attack!");
        /*
        GameObject ml = PhotonNetwork.Instantiate(this.melee.name, 
                                        weaponPos.position, 
                                        weaponPos.rotation,0, data);
        ml.transform.SetParent(weaponPos.transform);
        Destroy(ml.gameObject, 0.2f);*/
    }


    private void FollowCursor()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        if (!(UnityEngine.Camera.main is null))
        {
            Ray ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
            float hitDist = 0.0f;

            if (playerPlane.Raycast(ray, out hitDist))
            {
                Vector3 targetPoint = ray.GetPoint(hitDist);

                weaponHolder.transform.LookAt(new Vector3(targetPoint.x, weaponHolder.transform.position.y, targetPoint.z));
                Debug.DrawLine(ray.origin, targetPoint);
            }
        }
    }

    private void FixedUpdate()
    {
        if (!photonView.IsMine)
            return;
        FollowCursor();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(attackCollider.enabled);
            stream.SendNext(attackBox.enabled);
        }
        else
        {
            attackCollider.enabled = (bool) stream.ReceiveNext();
            attackBox.enabled = (bool) stream.ReceiveNext();
        }
    }
}