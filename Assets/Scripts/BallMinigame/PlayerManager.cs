﻿using System;
using Photon.Pun;
using UnityEngine;

namespace BallMinigame
{
    public class PlayerManager : MonoBehaviourPun
    {
        public PlayerStatus playerStatus;

        private Rigidbody rb;
        
        // Start is called before the first frame update
        void Start()
        {
            rb = this.GetComponent<Rigidbody>();
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if(!photonView.IsMine) return;
            //Collect item
            if (other.CompareTag("Item"))
            {
                Item tempItem = other.GetComponent<Item>();
                
                //Vaccine
                if (tempItem.itemType == ItemType.Vaccine)
                {
                    photonView.RPC("RPCured", RpcTarget.All);
                }
                
                //Mask
                if (tempItem.itemType == ItemType.Mask)
                {
                    photonView.RPC("RPCMasked", RpcTarget.All);
                }
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            if(!photonView.IsMine) return;
            
            //Collide with other player
            if (other.gameObject.CompareTag("Player"))
            {
                PlayerManager tempPlayerStat = other.gameObject.GetComponent<PlayerManager>();
                
                //Other is infectred
                if (tempPlayerStat.playerStatus == PlayerStatus.Infected)
                {
                    //Get infected
                    photonView.RPC("RPCInfected", RpcTarget.All);
                    
                    /*
                    //Already infected
                    if (this.playerStatus == PlayerStatus.Infected)
                    {
                        //Death
                        photonView.RPC("RPCDeath", RpcTarget.All);
                    }*/
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if(!photonView.IsMine) return;

            if (other.CompareTag("Player"))
            {
                PlayerManager tempPlayerStat = other.gameObject.GetComponent<PlayerManager>();
                if (tempPlayerStat.playerStatus == PlayerStatus.Infected)
                {
                    rb.velocity *= 0.1f;
                }
            }
        }

        public void Infected()
        {
            if (photonView != null)
                photonView.RPC("RPCInfected", RpcTarget.All);
            
        }

        public void Cured()
        {
            if (photonView != null)
                photonView.RPC("RPCured", RpcTarget.All);
        }

        public void Death()
        {
            if (photonView != null)
                photonView.RPC("RPCDeath", RpcTarget.All);
            //Also do something when death
        }

        [PunRPC]
        public void RPCInfected()
        {
            Debug.Log("Infected");
            playerStatus = PlayerStatus.Infected;
        }

        [PunRPC]
        public void RPCured()
        {
            Debug.Log("Cured");
            playerStatus = PlayerStatus.GoodHealth;
        }

        [PunRPC]
        public void RPCDeath()
        {
            Debug.Log("Death");
            playerStatus = PlayerStatus.Death;
        }

        [PunRPC]
        public void RPCMasked()
        {
            Debug.Log("Wear mask");
            playerStatus = PlayerStatus.WearMask;
        }
    }
}
