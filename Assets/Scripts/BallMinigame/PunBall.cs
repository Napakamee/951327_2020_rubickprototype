﻿using Photon.Pun;
using UnityEngine;

namespace BallMinigame
{
    public class PunBall : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        public float bulletForce = 20f;
        private bool isHit = false;
        int OwnerViewID = -1;
    
        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            // e.g. store this gameobject as this player's charater in Player.TagObject
            info.Sender.TagObject = this.gameObject;
            OwnerViewID = info.photonView.ViewID;

            //info.sender.TagObject = this.GameObject;
            Rigidbody bullet = GetComponent<Rigidbody>();
            // Add velocity to the bullet
            bullet.velocity = bullet.transform.forward * bulletForce;

            if (!photonView.IsMine)
                return;

            // Destroy the bullet after 10 seconds
            Destroy(this.gameObject, 10.0f);
        }

        private void OnCollisionEnter(Collision other)
        {
            if(!photonView.IsMine) return;

            if (other.gameObject.CompareTag("Player"))
            {
                Debug.Log("Hit other player.");
            
                PunUserNetControl tempOther = other.gameObject.GetComponent<PunUserNetControl>();
                if (tempOther != null)
                    Debug.Log("Attack to Other ViewID : " + tempOther.photonView.ViewID);

                PlayerManager tempPlayer = other.gameObject.GetComponent<PlayerManager>();
                if(tempPlayer != null)
                {
                    if (tempPlayer.playerStatus == PlayerStatus.GoodHealth && !isHit)
                    {
                        tempPlayer.Infected();
                        isHit = true;
                    }

                    if (tempPlayer.playerStatus == PlayerStatus.Infected && !isHit)
                    {
                        tempPlayer.Death();
                        isHit = true;
                    }
                }
            }
            Destroy(this.gameObject);
        }

        private void OnDestroy()
        {
            if (!photonView.IsMine) return;
        
            PhotonNetwork.Destroy(this.gameObject);
        }
    }
}