﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallMinigame
{
    public class Item : MonoBehaviour
    {
        public ItemType itemType;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Destroy(this.gameObject);
            }
        }
    }
}