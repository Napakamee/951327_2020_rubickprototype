﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletStat : MonoBehaviour
{
    public float attackForce = 20;
    public float atkSpd = 0.5f;
    public float attackDamage;
    public float attackRange;
    public bool isRange = false;
    
}
