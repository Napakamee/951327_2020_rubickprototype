﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

public class CustomizeController : MonoBehaviour
{
    [Header("Sprite Parts")]
    public SpriteRenderer[] spritePart;
    //public SpriteRenderer bodyPart;
    
    [Header("Sprite Options")]
    public Sprite[] hairOptions;
    public Sprite[] bodyOptions;
    
    [Space]
    public int[] index;

    //private int _partTemp;
    
    [Space]
    [SerializeField] private bool isEdit = false;
    
    public void Customize(string part , int indexpart)
    {
        if(part == PunSkinSetting.PLAYER_HAIR)
        {
            index[0] = indexpart;
        }
    }
    
    public void ApplyPart()
    {
        for (int i = 0; i < spritePart.Length; i++)
        {
            spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
        }
    }

    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return hairOptions[index]; break;
            case 1: return bodyOptions[index]; break;
        }
        

        return null;
    }
    
    private int ConvertIndexToOption(int part)
    {
        switch (part)
        {
            case 0: return hairOptions.Length; break;
            case 1: return bodyOptions.Length; break;
        }

        return 0;
    }

    #region Obsoleted

    //Obsoleted CustomizeUpdate
    /*//Only run when customize character
    private void CustomizeUpdate() 
    {
        
        for (int i = 0; i < hairOptions.Length; i++)
        {
            if (i == index[_partTemp])
            {
                //spritePart[_partTemp].sprite = hairOptions[i];
                switch (_partTemp)
                {
                    case 0: spritePart[0].sprite = hairOptions[i]; break;//Hair
                    case 1: spritePart[1].sprite = bodyOptions[i]; break;//Body
                }
            }
        }
        //Body
        /*for (int i = 0; i < bodyOptions.Length; i++)
        {
            if (i == index[1])
            {
                bodyPart.sprite = bodyOptions[i];
            }
        }
    }*/
    //Obsoleted NextPart
    /*public void OldNextPart(int part)
    {
        isEdit = true;
        index[part]++;
        
        if (index[part] >= hairOptions.Length)
        {
            index[part] = 0;
        }
        //_partTemp = part;
        ApplyPart();
    }*/


    #endregion
    
    public void NextPart(int part)
    {
        isEdit = true;
        index[part]++;
        
        if (index[part] >= ConvertIndexToOption(part))
        {
            index[part] = 0;
        }
        
        ApplyPart();
    }
    
    public void PreviousPart(int part)
    {
        isEdit = true;
        index[part]--;
        
        if (index[part] < 0)
        {
            index[part] = ConvertIndexToOption(part) - 1;
        }
        
        ApplyPart();
    }
    
    
    public void Save()
    {
        JSONObject playerAppearanceJson = new JSONObject();
        playerAppearanceJson.Add("Hair",index[0]);
        playerAppearanceJson.Add("Body",index[1]);
        
        Debug.Log(playerAppearanceJson.ToString());
        //Save Json
        string path = Directory.GetCurrentDirectory() + "/Assets/PlayerData/";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        File.WriteAllText(path + "PlayerAppearance.json", playerAppearanceJson.ToString());
    }

    public void Load()
    {
        isEdit = false;
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        spritePart[0].sprite = hairOptions[playerAppearanceJson["Hair"]]; index[0] = playerAppearanceJson["Hair"];
        spritePart[1].sprite = bodyOptions[playerAppearanceJson["Body"]]; index[1] = playerAppearanceJson["Body"];
    }
}
