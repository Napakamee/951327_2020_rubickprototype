﻿using System;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using SimpleJSON;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class SkinLoader : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    
    private Player _player;
    public SpriteRenderer[] spritePart;

    public Sprite[] hairOptions;
    public Sprite[] bodyOptions;
    public int[] index;
    [SerializeField] private bool newPlayerJoin = false;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        if (photonView.IsMine)
        {
            GetSkinIndex();
        }
    }

    private void Start()
    {
        ChangeSkinProperties();
    }

    /*
    public void UpdateSprite()
    {
        if (!photonView.IsMine)
            return;
        string path = Directory.GetCurrentDirectory() + "Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        hairPart.sprite = hairOptions[playerAppearanceJson["Hair"]]; index[0] = playerAppearanceJson["Hair"];
        bodyPart.sprite = bodyOptions[playerAppearanceJson["Body"]]; index[1] = playerAppearanceJson["Body"];
    }*/
    public void GetSkinIndex()
    {
        string path = Directory.GetCurrentDirectory() + "/Assets/PlayerData/PlayerAppearance.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerAppearanceJson = (JSONObject)JSON.Parse(jsonString);
        index[0] = playerAppearanceJson["Hair"];
        index[1] = playerAppearanceJson["Body"];
        Debug.Log("Hair: " + index[0]);
    }

    private void ChangeSkinProperties()
    {
        if (photonView.IsMine)
        {
            Hashtable skins = new Hashtable
            {
                {PunSkinSetting.PLAYER_HAIR, index[0]},
                {PunSkinSetting.PLAYER_BODY, index[1]}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(skins);
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertIndexToSprite(i, index[i]);
            }
        }
        else
        {
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
            /*_player = photonView.Owner;
            spritePart[0].sprite = hairOptions[(int)_player.CustomProperties[PunSkinSetting.PLAYER_HAIR]];*/
        }
    }
    
    public Sprite ConvertIndexToSprite(int part, int index)
    {
        switch (part)
        {
            case 0: return hairOptions[index]; break;
            case 1: return bodyOptions[index]; break;
        }

        return null;
    }

    Sprite ConvertPropsToSprite(int part, Hashtable props)
    {
        switch (part)
        {
            case 0: return hairOptions[(int) props[PunSkinSetting.PLAYER_HAIR]]; break;
            case 1: return bodyOptions[(int) props[PunSkinSetting.PLAYER_BODY]]; break;
        }
        return null;
    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        if (targetPlayer.ActorNumber == photonView.ControllerActorNr)
        {
            for (int i = 0; i < spritePart.Length; i++)
            {
                spritePart[i].sprite = ConvertPropsToSprite(i, changedProps);
            }
        }
        return;
    }
}
